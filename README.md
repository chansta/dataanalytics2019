# Taking Data Analytics for a Test Drive

## Introduction


This repository contains the materials for Curtin MasterClass on Data Analytics. It provides an overview on Big Data and Data Analytics without any pre-requisite on statistics or programming. These masterials facilitate a sequence of activities that demonstrate the typical process of a data analytics project. 

## Before we begin

While not strictly necessary, participants can maximise their learning experiences by getting their hands dirty with data. The following marterials may be helpful to boost your knowledge in data analytics software and basic statistics. 

### [R](http://www.r-project.org) and [Python][python]

The following materials on the basic of Python will be useful.  
1. For a distirbution of [Python][python], see [here](https://www.anaconda.com/distribution/). 
2. If you do not wish to install [Python][python] or [R](http://www.r-rpoject.org) on your system, you can start by [Kaggle](https://www.kaggle.com). It is an online service which allows you to experience Python and R without a local installation. 
3. For a genric introduction of Python for data analysis see [here](https://www.youtube.com/watch?v=a9UrKTVEeZA).
4.	Some of our demonstrations will utilise [Jupyter notebook](https://jupyter.org). See [here](https://www.youtube.com/watch?v=HW29067qVWk) for an introduction. 

### Statistics

1. [Crashed Course on Statistics #1](https://www.youtube.com/watch?v=sxQaBpKfDRk) - A crashed course on statitics. Good for both revision and new introduction.
2. [Crashed Course on Statistics #2](https://www.youtube.com/watch?v=tN9Xl1AcSv8) - A good follow up.
3. [Linear regression](https://www.youtube.com/watch?v=zPG4NjIkCjc) - Not the most exciting on regression analysi but it gets the points across.  


[python]: https://www.python.org

